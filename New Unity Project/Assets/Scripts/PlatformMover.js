﻿#pragma strict

private var Xpos : float;
private var Ypos : float;
private var max : boolean;

var Vert : boolean;
var maxAmount : int;
var step : float;
var X : float; // This is for Flipping the animation

function Start () {
	Xpos = transform.position.x;
	Ypos = transform.position.y;
	X = transform.localScale.x;
}

function Update () {
	
	//MOVING THE PLATFORM
	if(Vert){ // Vertical Movement
		if(!max) {
			transform.position.y += step;
		} else {
			transform.position.y -= step;
		}
	 } else { // Horizontal Movement
	 	if(!max) {
			transform.position.x += step;
		} else {
			transform.position.x -= step;
		}
	 }
	// SET MAX
	
	if(Vert){ // Vertical
		if(transform.position.y	>= Ypos + maxAmount) {
			max = true;
		} else if(transform.position.y <= Ypos) {
			max = false;
		}
	} else { // Horizontal
		if(transform.position.x	>= Xpos + maxAmount) {
			max = true;
			transform.localScale.x = -X;
		} else if(transform.position.x <= Xpos) {
			max = false;
			transform.localScale.x = X;
		}
	
		
	}
}