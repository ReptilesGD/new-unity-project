﻿#pragma strict

//Should font size dynamically be scaled up based on resolution?
//hmm... draw the logo halfway between the top and center of the screen? just on top of the background?

//Create a GUIEmpty, drag this script to it.
//Type in the name of the menu you want to appear.
//This is a list of all the menus used in the game.
public var menu="Main";
function menu_to_labels() {
	switch(menu) {
		case "Main":
			return [
				"Play",
				"Settings",
				"About",
				"Exit"
			];
			break;
		case "Pause":
			return [
				"Continue",
				"Store",
				"Load",
				"Save",
				"Return to Main Menu",
				"Exit"
			];
			break;
		default:
			print("Please see Menus.js:menu_to_labels() for a list of menus");
			//Yes, you have to put in a valid menu name - or add it up above
	}
}
//names of the buttons in the current menu
private var labels = menu_to_labels();

//Waste time every frame chopping the ends of the buttons?
private var recalculate_button_sizes = true;
//This function runs every frame, and handles menu stuff.
function OnGUI() {
	//TODO: make this not happen every frame
	if(recalculate_button_sizes) {
		//precalculate width so all the buttons will be as wide as the widest one
		var labelw:int = 0;
		var labelh:int = 0;
		for (var i=0; i<labels.Length; i++) {
			var size:Vector2 = GUI.skin.GetStyle("Button").CalcSize(GUIContent(labels[i]));
			labelw = Mathf.Max(labelw, size.x);
			labelh = Mathf.Max(labelh, size.y);
			//print(labels[i]+": ("+size.x+", "+size.y+")");
		}
		
		// the vertical distance between two buttons
		var spacer:int = labelh/2;
		// From a point, the distance down through a spacer and button
		var buttonh:int = spacer+labelh;
		
		//recalculate_button_sizes = false;
	}
	//center of bottom edge of game name image
	var x:int = Screen.width/2;
	var y:int = Screen.height/2;
	
	//create the logo
	size = GUI.skin.GetStyle("Label").CalcSize(GUIContent("Jetwings Revenge"));
	var logow:int=size.x;
	var logoh:int=size.y;
	GUI.Label(Rect(x-logow/2, Screen.height/4, logow, logoh), "Jetwings Revenge");
	//either the label or the box can draw the label or the image - nah, keep this out of the box.

	//button surrounder background box thing
	GUI.Box(Rect(x-(labelw/2+spacer), y, labelw+spacer*2, buttonh*labels.Length+spacer), "");
	//create the buttons
	for (i=0; i<labels.Length; i++) {
		if (GUI.Button(Rect(x-labelw/2, y+spacer+buttonh*i, labelw, labelh), labels[i])) {
			//print("pushed: "+labels[i]);
			//handle a button push from any menu - WHILE in the middle of drawing
			switch(labels[i]) {
				case "Play":
					Application.LoadLevel("sceneOne");
					break;
				/*case "Settings":
					break;
				case "About":
					break;*/
				case "Exit":
					//This section of code here is known as HaltAndCatchFire();
					//Unity claims to quit a program using Application.Quit();
					//but this doesn't work in the editor, online, or anywhere else it doesn't work.
					//Just pretend there's a return(0) here.
					#if UNITY_EDITOR
						UnityEditor.EditorApplication.isPlaying = false;
					#else
						if (Application.isWebPlayer) {
							//Application.ExternalEval ("history.back()");
							//avoid messing with someone's tabs, windows, or browser history.
							//Also don't crash the plugin so they have to reload.
							//Allow replaying the game even after an Exit.

							//After unity logo, show title screen.
							//After either a few seconds, or a click, show the main menu.
							Application.LoadLevel("TitleScreen");
							//Clicking Exit makes TitleScreen require a click to go back to the main menu
							//even if it previously only required a timeout.
						} else {
							Application.Quit();
						}
					#endif
					break;
				default:
					print("FIXME: Button handling of "+labels[i]);
			}
		}
	}
}