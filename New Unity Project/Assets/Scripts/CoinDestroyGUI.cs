﻿using UnityEngine;
using System.Collections;

public class CoinDestroyGui : MonoBehaviour {
	
	public GUIText cointext;
	
	public int curCoins;
	
	void Update(){
		cointext.text = "Money: " + curCoins;
	}
	
	void OnTriggerEnter(Collider col){
		if (col.gameObject.tag == "Penny") {
			Destroy (col.gameObject);
			curCoins++;
		}
		
		if (col.gameObject.tag == "Nickel") {
			Destroy (col.gameObject);
			curCoins = curCoins + 5;
		}
		
		if (col.gameObject.tag == "Dime") {
			Destroy (col.gameObject);
			curCoins = curCoins + 10;
		}

		if (col.gameObject.tag == "Quarter") {
			Destroy (col.gameObject);
			curCoins = curCoins + 25;
		}

		if (col.gameObject.tag == "dollar") {
			Destroy (col.gameObject);
			curCoins = curCoins + 100;
		}
	}
}