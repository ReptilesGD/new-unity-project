﻿//declare all variable types
#pragma strict

function OnMouseEnter() {
	transform.localScale = Vector3(2, 2, 2);
	renderer.material.color=Color.green;
}

function OnMouseExit() {
	transform.localScale = Vector3(1, 1, 1);
	renderer.material.color=Color.white;
}

function OnMouseUp() {
	switch(name) { // the object this script is attached to, which was clicked
		case "Play":
			Application.LoadLevel("sceneOne");
			break;
/*		case "Settings":
			//
			break;
		case "About":
			//
			break;*/
		case "Exit":
			HaltandCatchFire();
			break;
		default:
			print("Handle this menu button: " + name);
	}
}

function Update() {
	if(Input.GetKey(KeyCode.Escape) == true) {
		//do nothing/quit if first menu
		//go back to level if in-level menu
	}
}

//Unity didn't provide an all-in-one exit program command.
function HaltandCatchFire() {
	#if UNITY_EDITOR
		UnityEditor.EditorApplication.isPlaying = false;
	#else
		if (Application.isWebPlayer)
			Application.ExternalEval ("history.back()");

		Application.Quit();

		//it better have worked by now, and not gotten here if it did.
		print("Just let me die!");

	#endif
}