﻿using UnityEngine;
using System.Collections;

public class FixedVeloci : MonoBehaviour {

	Vector3 velocity = Vector3.zero;
	public float forwardSpeed = 1f;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void FixedUpdate () {
		velocity.x = forwardSpeed;

		transform.position += velocity * Time.deltaTime;
	}
}
