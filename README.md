# README #

#### RO - Reptiles Organization

### Introduction ###

* Game Name: TBD
* Game Type: 2D .; SideScroller ; Action / Strategy
* Platform: Unity 3d, javascript
* Publisher: Kongregate
* Monetization: TBD
* Team: Reptiles Organization
* Devs: Khonkhortisan and Snake12534
* Start Date: July 19, 2014
* Release Date: Estimated Fall 2014

### How do I get set up? ###

First, you need to clone the repository:

	git clone git@bitbucket.org:reptilesorganization/new-unity-project.git
	
You can perform and submit changes to the repo:

	git status     # See which files have changed
	git add        # Stage files so they can be commited
	git commit     # Save the changed files as a commit
	git push       # Push new commits to bitbucket
	git pull       # Get all the new changes from bitbucket

## RO Main Goals ##



Download Unity3D - Yes

Download Git - Yes

Master Collaboration - Yes

Start With a Main Menu - Yes

Start LevelOne - Yes

Finish LevelOne - No

Finish Levels 2-10 - No

Release Game - No



## Game Features Completions ##



### Snake12534 ###



Draw and Texture Enemies and Player for Level One



Animate Textures

Create LevelOne Community [add the obstacles, coins, and enemies in the strategic places]

FixedVelocity [script]

CoinGui+Destroy+includes coins++ on touch[script]

Draw Backgrounds

Parallax[script]

LevelRestrictionsScripts+GameObjects



### Khonkhortisan ###



(write here, khonkhortisan)



## Game Features Goals ##

Play Animation When Player Dies

Play Sound when Player dies

Play Menu when Player dies

Gun shooting script

Bullet script

Enemy shooting script

PlayerTakeDamage(oneLife)script

Money+ script for killing enemies

Level Selector GUI + script

Restart level + Gameover script and GUI

Shop GUI

Shop script

Shop items script

Shop items drawings

Story Mode implementation



[These are the main goals for now… more goals are to be added in the near future]



### Contribution guidelines ###

* Edits on other contributors' code are allowed if deemed with a motive.

### Who do I talk to? ###

* Snake12534 + Khonkhortisan
* depchetunite@gmail.com [Snake12534] & khonkhortisan@gmail.com [khonkhortisan]